How CSS render a website?
What happens to CSS when we load up a website?

1. Browser -> requesting by HTTP Request -> figure the right domain name service -> send request to the server
2. Server will response some resources (HTML, CSS, Javascript, ...) -> Browser
3. Browser -> load HTML -> parse HTML -> build DOM
4. When parse HTML -> load CSS (if exist) -> parse CSS -> CSSOM
5. Parse CSS will handles 2 process: resolve conflict css declaration (Cascade) -> process final css value
6. Render tree by Visual Formatting Model (algorithm will calculate a bunch of stuff like box model, floats, positioning,...)


