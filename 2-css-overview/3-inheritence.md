CSS BEHIND THE SCENES

* Inheritence: What you need to know
    - Inheritence passes the values for some specific properties from parents to childrens - for more maintainable code.
    - Propertes related to text are inherited: font-family, font-size, color, etc.
    - The computed value of a property is what get inherited, not declared value.
    - Inheritence of a property only works if no one declares a value for that property.
    - We can use 'inherit' keyword forces inheritence on a certain value.
    - The 'initial' keyword resets a property to its initial value.

