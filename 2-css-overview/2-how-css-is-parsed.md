CSS BEHIND THE SCENES

* How CSS value are processed
    - Why it important?
        + Because each time you use a unit other than pixels, like rem or vh, you need to know that it will ultimately be converted to pixels.
        + Also, knowing this stuff, will help you predict and control better which property takes which value in the end.
    - Process 6 steps: (example: <div class="width-half" style="width: 100%"></div>)
        -> Declared value (author declarations):                            50%, 100%
        -> Cascaded value (after the cascade):                              100%
        -> Specified value (defaulting if there is no cascaded value):      100%
        -> Computed value (converting relative value to absolute):          100%
        -> Used value (final calculation, based on layout):                 99.9px
        -> Actual value (browser and device restrictions):                  100px
    - Each property has a inital value, used if nothing is declared (and no inheritance)
    - Browser specify a root font-size for each page (usually 16px)

    ** How unit are converted from relative to absolute (px)
        - % (fonts) = x% * parent's computed font-size
        - % (length) = x% * parent's computed width (height, margin, padding,...)
        - Font-based:
            + em: use the PARENT or CURRENT ELEMENT as reference
            + rem: use the ROOT font-size
            + em (fonts) = x * PARENT computed font-size
            + em (length) = x * CURRENT ELEMENT computed font-size
            + rem = x * root(document's root font-size) computed font-size
        - Viewport-based
            + vh = x * 1% of view port height
            + vw = x * 1% of view port width