* How the Visual Formatting Model works?
    - Definition: Algorithm that calculates boxes and determine the layout of theses boxes, for each element in the render tree, in order to determine the final layout of the page.
        + Dimensions of the boxes: the box model.
        + Box type: inline, block & inline-block.
        + Positioning scheme: float and positioning.
        + Stacking context.
        + Other element in the render tree.
        + Viewport size, dimensions of images, etc.

** THE BOX MODEL
    - The CSS BOX MODEL is the foundation of layout of the Web, each element is represented as a rectagular box, with the box's content, padding, border, margin.


