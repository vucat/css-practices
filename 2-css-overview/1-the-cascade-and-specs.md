CSS BEHIND THE SCENES

* CSS rules
    - CSS has 2 parts: The Selector and The Declaration Block.
    - User Agent CSS is what the browser set to an element.
    - CSS declaration marked with !important have the highest priority.
    - But, only use !important as the last resource. It's better to correct specificities - for more maintainable code!
    - Inline styles will always have priority over styles in external stylesheets.
    - A selector that contains 1 ID is more specific than one with 1000 classes.
    - A selector that contains 1 class is more specific than one with 1000 elements.
    - The universal selector (*) has not specificity value (0,0,0,0);
    - Rely more on specifility than the order of selectors.
    - Rely on order when using 3rd-party stylesheets.

** The "C" in CSS (The Cascade)
    - The process of combining differents stylesheets and resolve conflicts between differents CSS rules and declarations, when more than one rule applies to a certain element.
    - IMPORTANCE > SPECIFICITY > SOURCE ORDER
        + Importance:
            #1. User !important declarations
            #2. Author !important declarations
            #3. Author declarations
            #4. User declarations
            #5. Default browser declarations
        + Specificity: (tính đặc trưng) *
            #1. Inline styles
            #2. IDs
            #3. Classes, pseudo-classes, attribute
            #4. Elements, psedo-elements
            => Calculate the selector (Inline, IDs, Classes, Elements);
            Example: nav#nav div.pull-right .button -> (0, 1, 2, 2) = [IDs level, 5]
        + Source order:
            The last declaration in the code will override all other declarations and will be applied.
