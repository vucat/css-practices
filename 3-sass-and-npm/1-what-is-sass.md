* What is SASS?
    - Definition: SASS is a CSS preprocessor, an extension of CSS that adds power and elegance to the basic language.

* How it work?
    SASS -> compiler -> CSS

* SASS features:
    - Variables: for reusable value such as colors, font-sizes, spacing, etc.
    - Nesting: to nest selector inside of one another, allow to write less code.
    - Operators: for mathematical operations right inside of CSS.
    - Partials and imports: to write CSS in different files and import them all in one single file.
    - Mixins: to write reusable pieces of CSS code.
    - Functions: similar to mixins, with the different that they produce a value that can be used.
    - Extends: to make different selectors inherit declarations that are common to all of them.
    - Control directives: to write complex code using conditionals and loops.

* SASS and SCSS
    - SASS syntax like a language: indentation sensitive and doesn't use curly braces and semicolons
    - SCSS which stands for Sassy CSS: preserves the way that original CSS looks like, which easier to use than the SASS syntax.

